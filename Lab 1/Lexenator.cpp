#include "Lexenator.h"
#include <map>
#include <iostream>
#include <sstream>
#include <iomanip>

//////////////
void Lexenator::reportErrEof(){
  okFlag = false;
  stringstream ss;
  ss << "Lexenator: Error(Line " << line <<", Column " << column << "): Unexpected EOF";
  errors.push_back(ss.str());
}
void Lexenator::reportErr(){
  okFlag = false;
  stringstream ss;
  ss << "Lexenator: Error(Line " << line <<", Column " << column << "): Illegal symbol '"<< current <<"'";
  errors.push_back(ss.str());
}

void Lexenator::iterateColumn(){
  column++;
  if(current == '\n'){
    line++;
    column = 1;
  }
}

int Lexenator::pushConstant(string cons){
  for( auto& c: data.constants){
    if(c.value == cons){
      return c.id;
    }
  }
  data.constants.push_back({.value = cons, .id = ++constCounter});
  return constCounter;
};

int Lexenator::pushIdent(string cons){
  for( auto& c: data.identifiers){
    if(c.value == cons){
      return c.id;
    }
  }
  data.identifiers.push_back({.value = cons, .id = ++identCounter});
  return identCounter;
};

void Lexenator::pushLexem(string value, int id, int line, int col) {
  data.lexems.push_back({value,id,line,column});
}

void Lexenator::printData(){
  for(int i = 0; i < data.lexems.size(); i++){
    cout << setw(3) << data.lexems[i].line
      << " "
      << setw(3) << data.lexems[i].col
      << " "
      << setw(4) << data.lexems[i].id 
      << " "
      << data.lexems[i].value
      << endl;
  }

  cout << "Constants:" << endl;
  for(auto cons: data.constants){
    cout << "\t" << setw(4) << cons.id 
      << " "
      << cons.value
      << endl;
  }
  cout << "Identifiers:" << endl;
  for(auto ident: data.identifiers){
    cout << "\t" << setw(4) << ident.id 
      << " "
      << ident.value
      << endl;
  }

}
/////////////////////


//All Forbidden by default
SymbolCategory SymbolCategories[128] = { Forbidden };
map<string, int> reserved = {
  {"PROCEDURE", 401},
  {"BEGIN", 402},
  {"END", 403},
  {"RETURN", 404},
};

Lexenator::Lexenator(){

  SymbolCategories[8] = Whitespace;
  SymbolCategories[9] = Whitespace;
  SymbolCategories[10] = Whitespace;
  SymbolCategories[13] = Whitespace;
  SymbolCategories[32] = Whitespace;
  for( int i = 'A'; i <= 'Z'; i++)
    SymbolCategories[i] = Letter;

  for( int i = '0'; i <= '9'; i++)
    SymbolCategories[i] = Digit;
  SymbolCategories[','] = Delimiter;
  SymbolCategories[';'] = Delimiter;
  SymbolCategories['('] = bcom;
  SymbolCategories[')'] = Delimiter;
  okFlag = true;
  constCounter = 500;
  identCounter = 1000;
}

void Lexenator::process(ifstream& input){
  column = 1;
  line = 1;
  this->input = &input;
  while(!input.eof()){
    current = input.get();
    if(current == -1)
      break;
    if(processChar())
      processChar();
  }
}

void Lexenator::processDigits(){
  string buf;
  buf.push_back(current);
  iterateColumn();
  while(!input->eof()){
    current = input->get();
    switch(SymbolCategories[current]){
      case Digit:
        buf.push_back(current);
        iterateColumn();
        continue;
      case Whitespace:
      case Delimiter:
        pushLexem(buf, pushConstant(buf), line, column);
        return;
      default:
        reportErr();
        return;
    }
  }
  pushLexem(buf, pushConstant(buf), line, column);
}

void Lexenator::processLetters(){
  string buf;
  buf.push_back(current);
  iterateColumn();
  while(!input->eof()){
    current = input->get();
    switch(SymbolCategories[current]){
      case Digit:
      case Letter:
        buf.push_back(current);
        iterateColumn();
        continue;
      default:
        if(reserved.count(buf)){
          pushLexem(buf, reserved[buf], line, column);
        }else{
          pushLexem(buf, pushIdent(buf), line, column);
        }
        return;
    }
  }
  if(reserved.count(buf)){
    pushLexem(buf, reserved[buf], line, column);
  }else{
    pushLexem(buf, pushIdent(buf), line, column);
  }
}

int Lexenator::processComment()
{
  bool inComment = false;
  int comState = 0;
  char next = input->get();
  if (next == '*'){
    inComment= true;
  }else{
      pushLexem(string(1, current), current, line, column);
      iterateColumn();
      current = next;
      return 1;
  }
  iterateColumn();
  current = next;
  iterateColumn();

  while(!input->eof()){
    current = input->get();
    switch(current){
      case '*':
        comState = 1;
        break;
      case ')':
        if (comState == 1){
          return 0;
        }else{
          comState = 0;
        }
        break;
      default:
        comState = 0;
        break;
    }
    iterateColumn();
  }
  reportErrEof();
  return 0;
}

int Lexenator::processChar()
{
  switch(SymbolCategories[current])
  {
    case Forbidden:
      reportErr();
      iterateColumn();
      return 0;
    case Whitespace:
      iterateColumn();
      return 0;
    case Digit:
      processDigits();
      return 1;
    case Letter:
      processLetters();
      return 1;
    case bcom:
      return processComment();
    case Delimiter:
      pushLexem(string(1, current), current, line, column);
      iterateColumn();
      return 0;
  }
  return 0;
}
