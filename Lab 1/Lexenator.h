#pragma once

#include <vector>
#include <string>
#include <fstream>

using namespace std;

struct Lexem {
  string value;
  int id;
  int line;
  int col;
};

struct Constant {
  string value;
  int id;
};

struct Identifier {
  string value;
  int id;
};

struct LexenatorData {
  vector<Lexem> lexems;
  vector<Constant> constants;
  vector<Identifier> identifiers;

};

enum SymbolCategory {
  Forbidden,
  Letter,
  Digit,
  Delimiter,
  Whitespace,
  bcom,
};

class Lexenator{

    LexenatorData data;
    bool okFlag;
    unsigned int line;
    unsigned int column;
    int constCounter;
    int identCounter;
    char current;
    ifstream* input;
    vector<string> errors;

    int processChar();
    void processDigits();
    void processLetters();
    int processComment();

    //Helpers
    int pushConstant(string);
    int pushIdent(string);
    void pushLexem(string, int, int,int);

    void iterateColumn();
    void reportErr();
    void reportErrEof();

  public:
    Lexenator();
    void process(ifstream&);
    bool isOk(){return okFlag;};
    LexenatorData getData(){return data;};
    vector<string> getErrors(){return errors;};
    void printData();

};
