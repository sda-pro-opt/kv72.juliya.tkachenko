#include <iostream>
#include <fstream>

#include "Lexenator.h"

int main(int argc, char** argv){

  if(argc < 2){
    std::cout << "Error: no input file" << std::endl;
    return -1;
  }
  
  std::ifstream file;
  file.open(argv[1]);

  Lexenator lex;
  lex.process(file);
  lex.printData();
  if(!lex.isOk()){
    for(auto err: lex.getErrors()){
      std::cout << err << std::endl;
    }
  }


  return 0;
}
